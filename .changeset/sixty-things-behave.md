---
"@fluidframework/container-runtime": minor
---

Summarizer.create(...) has been deprecated

The public static method `Summarizer.create(...)` has been deprecated and will be removed in a future major release. Creating a summarizer is not a publicly supported API. Please remove all usage of this method.
