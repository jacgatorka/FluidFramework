---
"@fluidframework/merge-tree": minor
---

Deprecate Stack, clone, combine, createMap, extend, extendIfUndefined, and matchProperties

This functionality was not intended for public export and will be removed in a future release.
