---
"@fluid-experimental/devtools-core": minor
"@fluid-experimental/property-shared-tree-interop": minor
"@fluid-experimental/tree-react-api": minor
"@fluid-experimental/tree2": minor
---

Rename "Value" Multiplicity and FieldKind

`Multiplicity.Value` has been renamed to `Multiplicity.Single` and `FieldKinds.value` has been renamed to `FieldKinds.required`.
