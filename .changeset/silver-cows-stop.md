---
"@fluidframework/container-loader": minor
"@fluidframework/location-redirection-utils": minor
---

Move location-redirection-utils APIs to container-loader

Moves the 2 package exports of `location-redirection-utils` to the `container-loader` package.

Exports from `location-redirection-utils` are now deprecated, and the package itself will be removed in an upcoming release.
