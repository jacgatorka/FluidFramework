---
"@fluidframework/merge-tree": minor
"@fluidframework/sequence": minor
---

Deprecate IntervalType.Nest, internedSpaces, RangeStackMap, refGetRangeLabels, refHasRangeLabel, and refHasRangeLabels

This functionality has poor test coverage and is largely unused. It will be removed in a future release.
