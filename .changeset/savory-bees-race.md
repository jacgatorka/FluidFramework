---
"@fluid-experimental/tree2": minor
---

Change forest summaries to include detached fields. This breaks existing documents.
